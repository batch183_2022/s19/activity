// console.log("Hello World");


// 3. Create a variable getCube and use the exponent operator to compute the cube of a number. (A cube is any number raised to 3)

let getCubes = [2];
let cubeNumbers = getCubes.map(num => num ** 3);

// 4. Using Template Literals, print out the value of the getCube variable with a message of The cube of <num> is…
console.log(`The cube of ${getCubes} is ${cubeNumbers}.`);

// 5. Create a variable address with a value of an array containing details of an address.

let address = ["258 Washington Ave NW", "California" , 90011];

// 6. Destructure the array and print out a message with the full address using Template Literals.
console.log(`I live at ${address[0]}, ${address[1]} ${address[2]}`);

// 7. Create a variable animal with a value of an object data type with different animal details as it’s properties.
const animal = {
 	name: "Lolong",
 	type: "saltwater crocodile",
 	details: {
 			weight: "1075 kgs",
 			measurement: "20 ft 3 in"
 			 }
 	};

// 8. Destructure the object and print out a message with the details of the animal using Template Literals.
let {name, type, details} = animal
console.log(`${name} was a ${type}. He weight at ${details.weight} with a measuremnt of ${details.measurement}.`);

// 9. Create an array of numbers.
let numbers = [1, 2, 3, 4, 5];

// 10. Loop through the array using forEach, an arrow function and using the implicit return statement to print out the numbers.
numbers.forEach(function(number){							
	console.log(`${number}`);
})

// 11. Create a variable reduceNumber and using the reduce array method and an arrow function console log the sum of all the numbers in the array.

 let reducedNumber = numbers.reduce(function(acc, cur){
        return acc + cur;
    })
    console.log(`${reducedNumber}`);

// 12. Create a class of a Dog and a constructor that will accept a name, age and breed as it’s properties.
class Dog{
	constructor(name, age, breed){
		this.name = name;
		this.age = age;
		this.breed = breed;
	}
}
const myDog = new Dog("Frankie", 5 , "Miniature Dashshund");
console.log(myDog);